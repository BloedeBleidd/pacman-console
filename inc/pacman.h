#pragma once

class Game;

class Pacman
{
private:
    int y{}, x{};
    int yOld{}, xOld{};
    int yInit{}, xInit{};
    int wait{};
    int left{};
    int score{};
    int hiScore{};
    int lives{};
    int super{};
    int killCount{};
    int color{};
    char dir{};
    char dirOld{};
    char icon{};

    Game *game;

public:
    Pacman(Game *const g);
    void move();
    void getDirection();
    bool testForCollision();
    void printScore(int);
    void printLives();
    void printKillScore();
    void dead();
    void show();
    void hide();

    int getY()     { return y;     }
    int getX()     { return x;     }
    int getYInit() { return yInit; }
    int getXInit() { return xInit; }
    int getLeft()  { return left;  }
    int getLives() { return lives; }
    int getSuper() { return super; }

    void setY(int y)         { this->y = y;   }
    void setX(int x)         { this->x = x;   }
    void setYInit(int y)     { yInit = y;     }
    void setXInit(int x)     { xInit = x;     }
    void setWait(int w)      { wait = w;      }
    void setLeft(int l)      { left = l;      }
    void setScore(int s)     { score = s;     }
    void setLives(int l)     { lives = l;     }
    void setSuper(int s)     { super = s;     }
    void setKillCount(int k) { killCount = k; }
    void setColor(int c)     { color = c;     }
    void setDirOld(char d)   { dirOld = d;    }
    void setIcon(char i)     { icon = i;      }
};
