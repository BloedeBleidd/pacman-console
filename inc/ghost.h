#pragma once

class Game;

class Ghost
{
private:
    int x{}, y{};
    int yInit{}, xInit{};
    int wait{};
    int color{};
    int colorInit{};
    char dir{};
    char dirOld{};
    char dirOpp{};
    char mode{};
    char modeOld{};
    char icon{};
    Game *game;

public:
    Ghost(Game *const g);
    void move(int, int);
    void targetObject(bool[4]);
    void randomDirection();
    bool testForCollision();
    void changeCoords();
    void getOpposite();
    void dead();
    void show();
    void hide();

    int getY()         { return y;         }
    int getX()         { return x;         }
    int getYInit()     { return yInit;     }
    int getXInit()     { return xInit;     }
    int getColor()     { return color;     }
    int getColorInit() { return colorInit; }
    char getMode()     { return mode;      }

    void setY(int y)         { this->y = y;   }
    void setX(int x)         { this->x = x;   }
    void setYInit(int y)     { yInit = y;     }
    void setXInit(int x)     { xInit = x;     }
    void setWait(int w)      { wait = w;      }
    void setColor(int c)     { color = c;     }
    void setColorInit(int c) { colorInit = c; }
    void setDirOpp(char d)   { dirOpp = d;    }
    void setMode(char m)     { mode = m;      }
    void setModeOld(char m)  { modeOld = m;   }
    void setIcon(char i)     { icon = i;      }
};

