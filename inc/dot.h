#pragma once

class Game;

class Dot
{
private:
    int x{}, y{};
    Game *game;

public:
    Dot(Game *const g);
    void print();

    void setY(int y) { this->y = y; }
    void setX(int x) { this->x = x; }
};
