#pragma once

#include "constants.h"

class Pacman;
class Ghost;
class Dot;

class Game
{
private:
    char level[LEVEL_HEIGHT][LEVEL_WIDTH];
    int oneUpTimer = ONE_UP_MAX;
    int oneUpColor = WHITE;
    int dotTimer = DOT_MAX;
    int dotColor = WHITE;
    int ghostModeTimer = MODE_MAX;

    Pacman* player;
    Ghost* ghosts[4];
    Dot* dots[4];

    void mainLoop();

public:
    Game(const char *title);
    ~Game();
    void go();

    void loadLevel();
    void nextLevel();
    void printReady();
    void printGameOver();

    void moveGhosts();
    void updateTimers();
    void checkForDeath();
    void showAll();
    void hideAll();
    void initAll();

    char getLevel(int y, int x) { return level[y][x];    }
    int getOneUpTimer()         { return oneUpTimer;     }
    int getOneUpColor()         { return oneUpColor;     }
    int getDotTimer()        	{ return dotTimer;    }
    int getDotColor()        	{ return dotColor;    }
    int getGhostModeTimer()     { return ghostModeTimer; }

    void setLevel(int y, int x, char c) { level[y][x] = c;    }
    void setOneUpTimer(int t)           { oneUpTimer = t;     }
    void setOneUpColor(int c)           { oneUpColor = c;     }
    void setDotTimer(int t)          	{ dotTimer = t;    }
    void setDotColor(int c)          	{ dotColor = c;    }
    void setGhostModeTimer(int t)       { ghostModeTimer = t; }
};

