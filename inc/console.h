#pragma once

void setWindowTitle(const char *);
void setWindowSize(int, int);
void setCursorVisibility(bool);
void setCursorPosition(int, int);
void setTextColor(int);
void setScreenColor(int);

