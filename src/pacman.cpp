#include <iostream>
#include <iomanip>
#include <conio.h>
#include <Windows.h>
#include <math.h>

#include "constants.h"

#include "game.h"
#include "pacman.h"
#include "console.h"

using namespace std;

Pacman::Pacman(Game *const g)
: game(g)
{
    hiScore = 0;
}

// check for user input every time the wait timer reaches 0
void Pacman::move()
{
    if (wait)
    {
        --wait;
    }
    else
    {
        getDirection();

        if (testForCollision() == false)
        {
            // replace old coordinates with a space
            setCursorPosition(yOld, xOld);
            cout << game->getLevel(yOld, xOld);

            // if the player picked up a pellet
            if (game->getLevel(y, x) != ' ')
            {
                int scoreInc;

                if (game->getLevel(y, x) == 'o')
                {
                    scoreInc = 50;
                    super = SUPER_MAX;
                }
                else
                {
                    scoreInc = 10;
                }

                printScore(scoreInc);
                game->setLevel(y, x, ' ');
                --left;
            }

            show();
            dirOld = dir;
            wait = PACMAN_MAX;
        }
    }
}

void Pacman::getDirection() {
    dir = 'x';
    // check if the user has entered 'w', 'a', 's' or 'd'
    if (_kbhit()) {
        dir = tolower(_getch());
    }
    // if not, try moving in the same direction as before
    if (!strchr(ALL_DIRS, dir)) {
        dir = dirOld;
    }
}

bool Pacman::testForCollision() {
    // save old coordinates
    xOld = x;
    yOld = y;
    // if the character in front of the player is a space, move in the appropriate direction
    switch (dir) {
    case 'a':
        // if travelling through the tunnel
        if (x == 0) {
            x = LEVEL_WIDTH - 1;
            icon = ICONS[1];
        }
        else if (strchr(NO_COLLISION_TILES, game->getLevel(y, x - 1))) {
            --x;
            icon = ICONS[1];
        }
        break;
    case 'd':
        // if travelling through the tunnel
        if (x == LEVEL_WIDTH - 1) {
            x = 0;
            icon = ICONS[3];
        }
        else if (strchr(NO_COLLISION_TILES, game->getLevel(y, x + 1))) {
            ++x;
            icon = ICONS[3];
        }
        break;
    case 'w':
        if (strchr(NO_COLLISION_TILES, game->getLevel(y - 1, x))) {
            --y;
            icon = ICONS[0];
        }
        break;
    case 's':
        if (strchr(NO_COLLISION_TILES, game->getLevel(y + 1, x))) {
            ++y;
            icon = ICONS[2];
        }
    }
    // if coordinates were not changed, there was a collision
    if (x == xOld && y == yOld) {
        return true;
    }
    return false;
}

void Pacman::printScore(int scoreInc) {
    // gain a life every time the score crosses a multiple of 10000
    if (score / 10000 < (score + scoreInc) / 10000) {
        ++lives;
        printLives();
    }
    score += scoreInc;
    setTextColor(WHITE);
    setCursorPosition(-2, 0);
    if (score == 0) {
        cout << setw(7) << "00";
    }
    else {
        cout << setw(7) << score;
    }
    if (score > hiScore) {
        hiScore = score;
        cout << setw(11) << hiScore;
    }
}

void Pacman::printLives() {
    setTextColor(color);
    setCursorPosition(LEVEL_HEIGHT, 2);
    for (int i = 1; i < lives; ++i) {
        cout << ICONS[1] << " ";
    }
    cout << " ";
}

void Pacman::printKillScore() {
    ++killCount;
    int scoreInc = 200 * (int)pow(2, killCount - 1);
    int length = (int)floor(log10(scoreInc)) + 1;
    int killX = x - 1;
    if (x == 0) {
        killX = x;
    }
    if (x > LEVEL_WIDTH - length) {
        killX = LEVEL_WIDTH - length;
    }
    setTextColor(CYAN);
    setCursorPosition(y, killX);
    cout << scoreInc;
    printScore(scoreInc);
    Sleep(750);
    setCursorPosition(y, killX);
    for (int i = killX; i < killX + length; ++i) {
        setTextColor(DARK_BLUE);
        if (game->getLevel(y, i) == char(250)) {
            setTextColor(WHITE);
        }
        if (game->getLevel(y, i) == 'o') {
            setTextColor(game->getDotColor());
        }
        cout << game->getLevel(y, i);
    }
    show();
}

void Pacman::dead() {
    Sleep(1000);
    game->hideAll();
    for (int i = 0; i < 8; ++i) {
        icon = ICONS[i % 4];
        show();
        Sleep(100);
    }
    hide();
    Sleep(500);
    --lives;
    if (lives != 0) {
        game->initAll();
        game->showAll();
        printLives();
        game->printReady();
    }
    else {
        game->printGameOver();
    }
}

void Pacman::show() {
    setTextColor(color);
    setCursorPosition(y, x);
    cout << icon;
}

void Pacman::hide() {
    setCursorPosition(y, x);
    cout << game->getLevel(y, x);
}
