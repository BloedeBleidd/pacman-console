#include <iostream>

#include "game.h"
#include "dot.h"
#include "console.h"

using namespace std;

Dot::Dot(Game *const g)
: game(g)
{

}

void Dot::print()
{
    setCursorPosition(y, x);
    cout << game->getLevel(y, x);
}
