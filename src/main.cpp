#include "game.h"

int main()
{
	const char *gametitle = "Console PACMAN";

    Game* game = new Game(gametitle);

    game->go();

    delete game;

    return 0;
}
