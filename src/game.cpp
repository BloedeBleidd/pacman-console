#include <pacman.h>
#include <iostream>
#include <Windows.h>

#include "constants.h"

#include "game.h"
#include "ghost.h"
#include "dot.h"
#include "console.h"

using namespace std;

Game::Game(const char *title)
{
    setWindowTitle(title);
    setWindowSize(LEVEL_HEIGHT + 4, LEVEL_WIDTH);
    setCursorVisibility(false);

    player = new Pacman(this);

    for (int i = 0; i < 4; ++i)
    {
        ghosts[i] = new Ghost(this);
        dots[i] = new Dot(this);
    }
}

Game::~Game()
{
    delete player;

    for (int i = 0; i < 4; ++i)
    {
        delete ghosts[i];
        delete dots[i];
    }
}

void Game::go()
{
    while (true)
    {
        mainLoop();
    }
}

void Game::mainLoop()
{
    player->setScore(0);
    player->setLives(3);
    bool gameOver = false;

    for (int levelNum = 1; levelNum <= 255; ++levelNum)
    {
        loadLevel();

        // while there are still dots on the screen,
        while (player->getLeft() != 0)
        {
            player->move();
            checkForDeath();

            if (!player->getLives())
            {
                gameOver = true;
                break;
            }

            moveGhosts();
            checkForDeath();

            if (!player->getLives())
            {
                gameOver = true;
                break;
            }

            updateTimers();
        }

        if (gameOver)
        {
            break;
        }

        nextLevel();
    }
}

void Game::loadLevel()
{
    char levelMap[LEVEL_HEIGHT][LEVEL_WIDTH + 1] =
    {
        "1555555555555555555555555552",
        "6............^^............6",
        "6.!%%@.!%%%@.^^.!%%%@.!%%@.6",
        "67^  ^.^   ^.^^.^   ^.^  ^86",
        "6.#%%$.#%%%$.#$.#%%%$.#%%$.6",
        "6..........................6",
        "6.!%%@.!@.!%%%%%%@.!@.!%%@.6",
        "6.#%%$.^^.#%%@!%%$.^^.#%%$.6",
        "6......^^....^^....^^......6",
        "355552.^#%%@ ^^ !%%$^.155554",
        "     6.^!%%$ #$ #%%@^.6     ",
        "     6.^^    B     ^^.6     ",
        "     6.^^ 155&&552 ^^.6     ",
        "555554.#$ 6      6 #$.355555",
        "      .   6I   C 6   .      ",
        "555552.!@ 6  P   6 !@.155555",
        "     6.^^ 35555554 ^^.6     ",
        "     6.^^          ^^.6     ",
        "     6.^^ !%%%%%%@ ^^.6     ",
        "155554.#$ #%%@!%%$ #$.355552",
        "6............^^............6",
        "6.!%%@.!%%%@.^^.!%%%@.!%%@.6",
        "6.#%@^.#%%%$.#$.#%%%$.^!%$.6",
        "69..^^.......X .......^^..06",
        "6%@.^^.!@.!%%%%%%@.!@.^^.!%6",
        "6%$.#$.^^.#%%@!%%$.^^.#$.#%6",
        "6......^^....^^....^^......6",
        "6.!%%%%$#%%@.^^.!%%$#%%%%@.6",
        "6.#%%%%%%%%$.#$.#%%%%%%%%$.6",
        "6..........................6",
        "3555555555555555555555555554"
    };

    char curChar;
    setTextColor(WHITE);
    setCursorPosition(-3, 3);
    cout << "1UP";
    setCursorPosition(-3, 9);
    cout << "HIGH SCORE";
    player->printScore(0);
    setCursorPosition(0, 0);
    player->setLeft(0);

    for (int y = 0; y < LEVEL_HEIGHT; ++y)
    {
        for (int x = 0; x < LEVEL_WIDTH; ++x)
        {
            curChar = levelMap[y][x];
            setTextColor(DARK_BLUE);

            switch (curChar)
            {
            case 'X':
                player->setYInit(y);
                player->setXInit(x);
                level[y][x] = ' ';
                break;
            case 'B':
                ghosts[BLINKY]->setYInit(y);
                ghosts[BLINKY]->setXInit(x);
                ghosts[BLINKY]->setColorInit(RED);
                ghosts[BLINKY]->setDirOpp('s');
                level[y][x] = ' ';
                break;
            case 'P':
                ghosts[PINKY]->setYInit(y);
                ghosts[PINKY]->setXInit(x);
                ghosts[PINKY]->setColorInit(MAGENTA);
                level[y][x] = ' ';
                break;
            case 'I':
                ghosts[INKY]->setYInit(y);
                ghosts[INKY]->setXInit(x);
                ghosts[INKY]->setColorInit(CYAN);
                level[y][x] = ' ';
                break;
            case 'C':
                ghosts[CLYDE]->setYInit(y);
                ghosts[CLYDE]->setXInit(x);
                ghosts[CLYDE]->setColorInit(YELLOW);
                level[y][x] = ' ';
                break;
            case '7':
                dots[0]->setY(y);
                dots[0]->setX(x);
                setTextColor(WHITE);
                level[y][x] = 'o';
                player->setLeft(player->getLeft() + 1);
                break;
            case '8':
                dots[1]->setY(y);
                dots[1]->setX(x);
                setTextColor(WHITE);
                level[y][x] = 'o';
                player->setLeft(player->getLeft() + 1);
                break;
            case '9':
                dots[2]->setY(y);
                dots[2]->setX(x);
                setTextColor(WHITE);
                level[y][x] = 'o';
                player->setLeft(player->getLeft() + 1);
                break;
            case '0':
                dots[3]->setY(y);
                dots[3]->setX(x);
                setTextColor(WHITE);
                level[y][x] = 'o';
                player->setLeft(player->getLeft() + 1);
                break;
            case '.':
                setTextColor(WHITE);
                level[y][x] = char(250);
                player->setLeft(player->getLeft() + 1);
                break;
            case ' ':
                level[y][x] = curChar;
                break;
            case '&':
                setTextColor(WHITE);
                curChar = '%';
            }

            if (curChar == '1')
            {
                level[y][x] = char(201);
            }
            else if (curChar == '2')
            {
                level[y][x] = char(187);
            }
            else if (curChar == '3')
            {
                level[y][x] = char(200);
            }
            else if (curChar == '4')
            {
                level[y][x] = char(188);
            }
            else if (curChar == '5')
            {
                level[y][x] = char(205);
            }
            else if (curChar == '6')
            {
                level[y][x] = char(186);
            }
            else if (curChar == '!')
            {
                level[y][x] = char(218);
            }
            else if (curChar == '@')
            {
                level[y][x] = char(191);
            }
            else if (curChar == '#')
            {
                level[y][x] = char(192);
            }
            else if (curChar == '$')
            {
                level[y][x] = char(217);
            }
            else if (curChar == '%')
            {
                level[y][x] = char(196);
            }
            else if (curChar == '^')
            {
                level[y][x] = char(179);
            }

            cout << level[y][x];
        }

        setCursorPosition(y + 1, 0);
    }

    initAll();
    showAll();
    player->printLives();
    printReady();
}

void Game::nextLevel()
{
    Sleep(1000);
    hideAll();
    setCursorPosition(12, 13);
    cout << "  ";

    for (int i = 0; i < 4; ++i)
    {
        setScreenColor(WHITE);
        player->show();
        Sleep(200);
        setScreenColor(DARK_BLUE);
        player->show();
        Sleep(200);
    }

    setCursorPosition(0, 0);
    initAll();
}

void Game::printReady()
{
    setTextColor(YELLOW);
    setCursorPosition(17, 11);
    cout << "READY!";
    Sleep(2000);
    setCursorPosition(17, 11);
    cout << "      ";
}

void Game::printGameOver()
{
    setCursorPosition(17, 9);
    setTextColor(RED);
    cout << "GAME  OVER";
    Sleep(1000);
}

void Game::moveGhosts()
{
    // check for ghost mode changes
    if (player->getSuper() == SUPER_MAX)
    {
        player->setKillCount(0);

        for (int i = 0; i < 4; ++i)
        {
            if (ghosts[i]->getMode() != 'd')
            {
                ghosts[i]->setColor(BLUE);
            }

            if (ghosts[i]->getMode() == 's' || ghosts[i]->getMode() == 'c')
            {
                ghosts[i]->setMode('r');
            }
        }

        showAll();
    }

    if (player->getLeft() == 235 && ghosts[PINKY]->getMode() == 'w')
    {
        ghosts[PINKY]->setMode('e');
    }

    if (player->getLeft() == 200 && ghosts[INKY]->getMode() == 'w')
    {
        ghosts[INKY]->setMode('e');
    }

    if (player->getLeft() == 165 && ghosts[CLYDE]->getMode() == 'w')
    {
        ghosts[CLYDE]->setMode('e');
    }

    for (int i = 0; i < 4; ++i)
    {
        ghosts[i]->move(player->getY(), player->getX());
    }

    showAll();
}

void Game::updateTimers()
{
    // handle super pacman
    if (player->getSuper())
    {
        player->setSuper(player->getSuper() - 1);

        if (player->getSuper() <= 112 && player->getSuper() % 28 == 0)
        {
            for (int i = 0; i < 4; ++i)
            {
				if (ghosts[i]->getColor() == BLUE)
				{
					ghosts[i]->setColor(WHITE);
				}
            }

            showAll();
        }

        if (player->getSuper() <= 98 && (player->getSuper() + 14) % 28 == 0)
        {
            for (int i = 0; i < 4; ++i)
            {
                if (ghosts[i]->getColor() == WHITE && ghosts[i]->getMode() != 'd' && ghosts[i]->getMode() != 'n') {
                    ghosts[i]->setColor(BLUE);
                }
            }

            showAll();
        }

        if (!player->getSuper())
        {
            for (int i = 0; i < 4; ++i)
            {
                if (ghosts[i]->getMode() != 'd' && ghosts[i]->getMode() != 'n')
                {
                    ghosts[i]->setColor(ghosts[i]->getColorInit());
                }

                if (ghosts[i]->getMode() == 'r')
                {
                    ghosts[i]->setModeOld(ghosts[i]->getMode());
                    ghosts[i]->setMode('c');
                }
            }

            showAll();
        }
    }
    // handle flashing 1UP
    if (oneUpTimer)
    {
        --oneUpTimer;
    }
    else
    {
        if (oneUpColor == WHITE)
        {
            oneUpColor = BLACK;
        }
        else
        {
            oneUpColor = WHITE;
        }

        setTextColor(oneUpColor);
        setCursorPosition(-3, 3);
        cout << "1UP";
        oneUpTimer = ONE_UP_MAX;
    }

    // handle flashing super dots
    if (dotTimer)
    {
        --dotTimer;
    }
    else
    {
        if (dotColor == WHITE)
        {
            dotColor = BLACK;
        }
        else
        {
            dotColor = WHITE;
        }

        setTextColor(dotColor);

        for (int i = 0; i < 4; ++i)
        {
            dots[i]->print();
        }

        showAll();
        dotTimer = DOT_MAX;
    }
    // handle ghost chase/scatter mode
    if (ghostModeTimer)
    {
        --ghostModeTimer;

        if (ghostModeTimer == MODE_MAX / 4)
        {
            for (int i = 0; i < 4; ++i)
            {
                if (ghosts[i]->getMode() == 'c')
                {
                    ghosts[i]->setMode('s');
                }
            }
        }
    }
    else
    {
        for (int i = 0; i < 4; ++i)
        {
            if (ghosts[i]->getMode() == 's')
            {
                ghosts[i]->setMode('c');
            }
        }

        ghostModeTimer = MODE_MAX;
    }

    Sleep(7);
}

void Game::checkForDeath()
{
    for (int i = 0; i < 4; ++i)
    {
        if (player->getX() == ghosts[i]->getX() && player->getY() == ghosts[i]->getY() && ghosts[i]->getMode() != 'd' && ghosts[i]->getMode() != 'n')
        {
            if (ghosts[i]->getMode() != 'r')
            {
                player->dead();
            }
            else
            {
                player->printKillScore();
                ghosts[i]->dead();
            }
        }
    }
}

void Game::showAll()
{
    player->show();

    for (int i = 0; i < 4; ++i)
    {
        ghosts[i]->show();
    }
}

void Game::hideAll()
{
    player->hide();

    for (int i = 0; i < 4; ++i)
    {
        ghosts[i]->hide();
    }
}

void Game::initAll()
{
    player->setY(player->getYInit());
    player->setX(player->getXInit());
    player->setColor(YELLOW);
    player->setIcon(ICONS[1]);
    player->setDirOld('a');
    player->setWait(0);
    player->setSuper(0);

    for (int i = 0; i < 4; ++i)
    {
        ghosts[i]->setY(ghosts[i]->getYInit());
        ghosts[i]->setX(ghosts[i]->getXInit());
        ghosts[i]->setColor(ghosts[i]->getColorInit());
        ghosts[i]->setMode('w');
        ghosts[i]->setWait(0);
        ghosts[i]->setIcon(GHOST_ICON);
    }

    ghosts[BLINKY]->setMode('c');
    ghosts[BLINKY]->setModeOld('c');

    if (player->getLeft() <= 235)
    {
        ghosts[PINKY]->setMode('e');
    }

    if (player->getLeft() <= 200)
    {
        ghosts[INKY]->setMode('e');
    }

    if (player->getLeft() <= 165)
    {
        ghosts[CLYDE]->setMode('e');
    }
}
